## InfluxDB
Download and install InfluxDB:
```
wget https://dl.influxdata.com/influxdb/releases/influxdb_1.8.0_amd64.deb
sudo dpkg -i influxdb_1.8.0_amd64.deb
```

To start the service, run:
```
sudo service influxdb start
```

Log into the InfluxDB console
```
influx
```

Create the databases and the users we need for the demo
```
CREATE DATABASE "kafka-metrics"
CREATE DATABASE "flink-metrics"
CREATE DATABASE "flink-results"

CREATE USER "kafka" WITH PASSWORD 'kafka'
CREATE USER "flink" WITH PASSWORD 'flink'
CREATE USER "grafana" WITH PASSWORD 'grafana'

GRANT ALL ON "kafka-metrics" TO "kafka"
GRANT READ ON "kafka-metrics" TO "grafana" 
GRANT ALL ON "flink-metrics" TO "flink"
GRANT READ ON "flink-metrics" TO "grafana" 
GRANT ALL ON "flink-results" TO "flink"
GRANT READ ON "flink-results" TO "grafana" 
```

Create the retention policy to keep the data for 2 hours
```
CREATE RETENTION POLICY "two_hours" ON "flink-metrics" DURATION 2h REPLICATION 1 DEFAULT
```

## Kafka
Download and unzip Kafka:
```
wget http://mirror.easyname.ch/apache/kafka/2.5.0/kafka_2.12-2.5.0.tgz
tar xzf kafka_2.12-2.5.0.tgz
cd kafka_2.12-2.5.0
```

Download Jolokia from [https://search.maven.org/remotecontent?filepath%3Dorg/jolokia/jolokia-jvm/1.6.2/jolokia-jvm-1.6.2-agent.jar] and and copy it in kafka_2.12-2.5.0/libs. Rename it in jolokia-agent.jar
```
cd libs
mv jolokia-jvm-1.6.2-agent.jar jolokia-1.6.2.jar
```

Start Zookeeper from kafka folder
```
bin/zookeeper-server-start.sh config/zookeeper.properties
```

Start Kafka with Jolokia
```
 JMX_PORT="9999" RMI_HOSTNAME="localhost" KAFKA_JMX_OPTS="-javaagent:/home/soheila/summerSchool/kafka_2.12-2.5.0/libs/jolokia-agent.jar=port=8778,host=$RMI_HOSTNAME -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Djava.rmi.server.hostname=$RMI_HOSTNAME -Dcom.sun.management.jmxremote.rmi.port=$JMX_PORT" bin/kafka-server-start.sh config/server.properties
```

## Setup Telegraf
Download and install Telegraf:
```
wget https://dl.influxdata.com/telegraf/releases/telegraf_1.14.3-1_amd64.deb
sudo dpkg -i telegraf_1.14.3-1_amd64.deb
```

Create the configuraiton file to write Kafka metrics to InfluxDB
```
mkdir telegraf
cd telegraf
touch telegraf.toml
```

Add the configuration in telegraf.toml
```
# OUTPUTS
[outputs]
[outputs.influxdb]
    # The full HTTP endpoint URL for your InfluxDB instance
    url = "http://localhost:8086"
    # The target database for metrics. This database must already exist
    database = "kafka-metrics"

[inputs]
[[inputs.jolokia2_agent]]
  name_prefix = "kafka_"

  urls = ["http://localhost:8778/jolokia"]

  [[inputs.jolokia2_agent.metric]]
    name         = "controller"
    mbean        = "kafka.controller:name=*,type=*"
    field_prefix = "$1."

  [[inputs.jolokia2_agent.metric]]
    name         = "replica_manager"
    mbean        = "kafka.server:name=*,type=ReplicaManager"
    field_prefix = "$1."

  [[inputs.jolokia2_agent.metric]]
    name         = "purgatory"
    mbean        = "kafka.server:delayedOperation=*,name=*,type=DelayedOperationPurgatory"
    field_prefix = "$1."
    field_name   = "$2"

  [[inputs.jolokia2_agent.metric]]
    name     = "client"
    mbean    = "kafka.server:client-id=*,type=*"
    tag_keys = ["client-id", "type"]

  [[inputs.jolokia2_agent.metric]]
    name         = "request"
    mbean        = "kafka.network:name=*,request=*,type=RequestMetrics"
    field_prefix = "$1."
    tag_keys     = ["request"]

  [[inputs.jolokia2_agent.metric]]
    name         = "topics"
    mbean        = "kafka.server:name=*,type=BrokerTopicMetrics"
    field_prefix = "$1."

  [[inputs.jolokia2_agent.metric]]
    name         = "topic"
    mbean        = "kafka.server:name=*,topic=*,type=BrokerTopicMetrics"
    field_prefix = "$1."
    tag_keys     = ["topic"]

  [[inputs.jolokia2_agent.metric]]
    name       = "partition"
    mbean      = "kafka.log:name=*,partition=*,topic=*,type=Log"
    field_name = "$1"
    tag_keys   = ["topic", "partition"]

  [[inputs.jolokia2_agent.metric]]
    name       = "partition"
    mbean      = "kafka.cluster:name=UnderReplicated,partition=*,topic=*,type=Partition"
    field_name = "UnderReplicatedPartitions"
    tag_keys   = ["topic", "partition"]
```

To start the service, run first a test:
```
telegraf -config telegraf.toml -test
```

If it works, start it:
```
telegraf -config telegraf.toml
```


## Setup Flink
Download and unzip Flink:
```
wget http://mirror.easyname.ch/apache/flink/flink-1.10.1/flink-1.10.1-bin-scala_2.11.tgz
tar xf flink-1.10.1-bin-scala_2.11.tgz
cd flink-1.10.1
```

Add the connector to InfluxDB to Flink
```
cp opt/flink-metrics-influxdb-1.10.1.jar lib/
```

Edit conf/flink-conf.yaml and add those lines at the end:
```
metrics.reporter.influxdb.class: org.apache.flink.metrics.influxdb.InfluxdbReporter
metrics.reporter.influxdb.host: localhost
metrics.reporter.influxdb.port: 8086
metrics.reporter.influxdb.db: flink-metrics
metrics.reporter.influxdb.username: flink
metrics.reporter.influxdb.password: flink
metrics.reporter.influxdb.retentionPolicy: two_hours
metrics.reporter.influxdb.consistency: ANY
metrics.reporter.influxdb.connectTimeout: 60000
metrics.reporter.influxdb.writeTimeout: 60000
```

Start Flink
```
bin/start-cluster.sh
```

## Setup the wiki to kafka connector
```
git clone https://gitlab.com/soheilade/summer-school-w2k.git
```

To install:
```
python3 -m venv ~/venvs/wikipedia
source ~/venvs/wikipedia/bin/activate
pip install SSEClient
pip install confluent_kafka
python3 wikipedia_to_kafka.py
```

To run:
```
source ~/venvs/wikipedia/bin/activate
python3 wk2.py
```

## Setup the wiki edits analyser
```
git clone https://gitlab.com/soheilade/summer-school-flink.git
cd summer-school-flink
mvn package
```

Open the GUI at http://localhost:8081

Load the job, stored in target: Submit new Job -> Add New -> summer-school-flink/target/
wiki-analyser-1.0.jar -> press -> Submit

Run it

## Setup Grafana
```
mkdir grafana
wget https://dl.grafana.com/oss/release/grafana-6.3.3.linux-amd64.tar.gz
tar xf grafana-6.3.3.linux-amd64.tar.gz
cd grafana-6.3.3
bin/grafana-server start
```