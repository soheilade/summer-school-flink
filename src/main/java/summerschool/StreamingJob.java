/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package summerschool;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.api.java.tuple.Tuple6;
import
org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.JsonNode;
import
org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import
org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import
org.apache.flink.streaming.api.windowing.assigners.SlidingProcessingTimeWindows;
import
org.apache.flink.streaming.api.windowing.assigners.TumblingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.connectors.influxdb.InfluxDBConfig;
import org.apache.flink.streaming.connectors.influxdb.InfluxDBPoint;
import org.apache.flink.streaming.connectors.influxdb.InfluxDBSink;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import
org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer.Semantic;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.flink.util.Collector;

public class StreamingJob {
	public static void main(String[] args) throws Exception {
		// set up the streaming execution environment
		final StreamExecutionEnvironment env =
				StreamExecutionEnvironment.getExecutionEnvironment();
		env.setParallelism(1);
		// env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);


		Properties properties = new Properties();
		properties.setProperty("bootstrap.servers", "localhost:9092"); //server
		properties.setProperty("group.id", "wikipedia-edits");

		FlinkKafkaConsumer<String> fkc = new FlinkKafkaConsumer<>(
				"wikipedia-edits",
				new SimpleStringSchema(),
				properties);
		fkc.setStartFromLatest();

		DataStream<Tuple5<String,String,String,Boolean,String>> stream = env
				.addSource(fkc)
				.map(s -> {
					ObjectMapper om = new ObjectMapper();
					JsonNode root = om.readValue(s, JsonNode.class);
					return new Tuple5<String,String,String,Boolean,String>(
							root.get("server_name").asText(),
							root.get("user").asText(),
							root.get("type").asText(),
							root.get("bot").asBoolean(),
							root.get("title").asText()
							);
				}).returns(new TypeHint<Tuple5<String,String,String,Boolean,String>>() {});

		//Create the sink to send to InfluxDB the results of the analysys
		InfluxDBConfig influxDBConfig =
				InfluxDBConfig.builder("http://localhost:8086", "flink", "flink",
						"flink-results").build();
		InfluxDBSink idbs = new InfluxDBSink(influxDBConfig);

		// Analysis 1: numbers of bots and humans editing Wikipedia en
		DataStream<Tuple2<String,Integer>> stream1 = stream
				.map(t -> new
						Tuple6<String,String,String,Boolean,String,Integer>(t.f0, t.f1, t.f2,
								t.f3, t.f4, 1)).returns(new
										TypeHint<Tuple6<String,String,String,Boolean,String,Integer>>() {})
				.keyBy(3)
				.window(TumblingProcessingTimeWindows.of(Time.seconds(10)))
				.sum(5)
				.project(3,5)
				.map(t -> (Boolean)t.getField(0) ? new
						Tuple2<String,Integer>("bot", (Integer)t.getField(1)) : new
						Tuple2<String,Integer>("human", (Integer)t.getField(1))).returns(new
								TypeHint<Tuple2<String,Integer>>() {});

		//Create the sink to send to Kafka the results of the analysys
		FlinkKafkaProducer<Tuple2<String,Integer>> prod1 = new
				FlinkKafkaProducer<Tuple2<String,Integer>>(
						"enwiki-bots",
						((r,t) -> new ProducerRecord<byte[],
								byte[]>("enwiki-bots", r.toString().getBytes())),
						properties,
						Semantic.AT_LEAST_ONCE);
		stream1.addSink(prod1);

		//analysis to convert processed stream into tuples(i.e, tags and fields) of influxDB
		stream1
		.map(t -> {
			Map<String,String> tags = new HashMap<>();
			Map<String,Object> fields = new HashMap<>();
			tags.put("type", t.f0.toString());
			fields.put("value", t.f1);
			return new InfluxDBPoint("analysis-1",
					System.currentTimeMillis(), tags, fields);
		})
		.addSink(idbs);

		// Analysis 2: most edited wikimedia projects
		DataStream<Tuple2<String,Integer>> stream2 = stream
				.map(t -> new
						Tuple6<String,String,String,Boolean,String,Integer>(t.f0, t.f1, t.f2,
								t.f3, t.f4, 1))
				.returns(new
						TypeHint<Tuple6<String,String,String,Boolean,String,Integer>>() {})
				.keyBy(0)
				.window(SlidingProcessingTimeWindows.of(Time.seconds(30),
						Time.seconds(5)))
				.sum(5)
				.project(0,5);

		//create sink for kafka
		FlinkKafkaProducer<Tuple2<String,Integer>> prod2 = new
				FlinkKafkaProducer<Tuple2<String,Integer>>(
						"wiki-pop",
						((r,t) -> new ProducerRecord<byte[],
								byte[]>("wiki-pop", r.toString().getBytes())),
						properties,
						Semantic.EXACTLY_ONCE);

		stream2.addSink(prod2);

		//analysis to convert processed stream into tuples(i.e, tags and fields) of influxDB
		stream2
		.map(t -> {
			Map<String,String> tags = new HashMap<>();
			Map<String,Object> fields = new HashMap<>();
			tags.put("wiki", t.f0.toString());
			fields.put("value", t.f1);
			return new InfluxDBPoint("analysis-2",
					System.currentTimeMillis(), tags, fields);
		})
		.addSink(idbs);

/*
		//Analysis 3: most editted wiki media categories       
		String wiki = "it.wikipedia.org";

		DataStream<Tuple2<String,Integer>> stream3 = stream
				.filter(t -> !t.f3)
				.filter(t -> t.f0.equals(wiki))
				.flatMap((Tuple5<String,String,String,Boolean,String> value, Collector<Tuple2<String, Integer>> out) -> {
					try{
						Query q = QueryFactory.create(
								"PREFIX dcterms: <http://purl.org/dc/terms/> "
										+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "
										+ "PREFIX wdt: <http://www.wikidata.org/prop/direct/> "
										+ "PREFIX schema: <http://schema.org/> "
										+ "SELECT ?name "
										+ "WHERE {"
										+ "  <https://"+ wiki + "/wiki/" + value.f4.replace(" ", "_")+"> schema:about ?item. "
										+ "  ?item wdt:P31 ?cat ."
										+ "  ?cat rdfs:label ?name ."
										+ "  FILTER (lang(?name) = \"en\")"
										+ "} LIMIT 5");

						QueryExecution exec = QueryExecutionFactory.sparqlService("https://query.wikidata.org/bigdata/namespace/wdq/sparql", q);
						exec.setTimeout(3000);

						ResultSet results = exec.execSelect();
						while(results.hasNext()) {
							String cat = results.next().get("name").toString();
							Tuple2<String,Integer> ret = new Tuple2<>();
							ret.setField(cat, 0);
							ret.setField(1, 1);
							out.collect(ret);
						}
					} catch(Exception e){
						e.printStackTrace();
					}
				}).returns(new TypeHint<Tuple2<String,Integer>>(){})
				.keyBy(0)
				.window(TumblingProcessingTimeWindows.of(Time.seconds(10)))
				.sum(1);

		stream3
		.map(t -> {
			Map<String,String> tags = new HashMap<>();
			Map<String,Object> fields = new HashMap<>();
			tags.put("category", t.f0.toString());
			fields.put("edits", t.f1);
			return new InfluxDBPoint("analysis-3", System.currentTimeMillis(), tags, fields);
		})
		.addSink(idbs);
*/
		//--------------------------------------------------

		try{
			env.execute();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}